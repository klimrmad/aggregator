<?php

namespace App\Cache;

use App\Cache\Exceptions\CacheNotFoundException;
use App\Cache\Exceptions\CacheTimeoutException;
use App\Cache\Exceptions\CacheWriteException;

class FileCache implements Cache
{
    /**
     * @var int
     */
    private $ttl;

    /**
     * @var string
     */
    private $prefix;

    /**
     * FileCache constructor.
     *
     * @param string $prefix
     * @param int    $ttl
     */
    public function __construct(string $prefix, int $ttl)
    {
        $this->prefix = $this->cleanKey($prefix);
        $this->ttl    = $ttl > 0 ? $ttl : 0;
    }

    /**
     * @inheritdoc
     */
    public function get(string $key): string
    {
        $this->checkTtl($key);
        return $this->doGet($key);
    }

    /**
     * @param string $key
     */
    public function touch(string $key): void
    {
        $this->doTouch($key);
    }

    /**
     * @inheritdoc
     */
    public function set(string $key, string $data): void
    {
        $this->doSet($key, $data);
    }

    /**
     * @param string $key
     *
     * @throws CacheNotFoundException
     * @throws CacheTimeoutException
     */
    private function checkTtl(string $key): void
    {
        if (!$this->ttl) {
            return;
        }

        $mtime = (int) $this->getStatOption($key, 'mtime');
        if (time() - $mtime > $this->ttl) {
            throw new CacheTimeoutException('Cached data timed out');
        }
    }

    /**
     * @param string $key
     *
     * @return string
     * @throws CacheNotFoundException
     */
    private function doGet(string $key): string
    {
        $data = file_get_contents($this->getFileName($key), false);
        if (false === $data) {
            throw new CacheNotFoundException('Cached data not found');
        }

        return $data;
    }

    /**
     * @param string $key
     */
    private function doTouch(string $key): void
    {
        touch($this->getFileName($key));
    }

    /**
     * @param string $key
     * @param string $data
     *
     * @throws CacheWriteException
     */
    private function doSet(string $key, string $data): void
    {
        $bytesWritten = file_put_contents($this->getFileName($key), $data);
        if (false === $bytesWritten) {
            throw new CacheWriteException('Cached data write failure');
        }
    }

    /**
     * @see http://php.net/manual/en/function.stat.php
     *
     * @param string $key
     * @param string $option
     *
     * @return string
     * @throws CacheNotFoundException
     */
    private function getStatOption(string $key, string $option): string
    {
        $stat = stat($this->getFileName($key));
        if (false === $stat) {
            throw new CacheNotFoundException('Cached data not found');
        }

        if (!array_key_exists($option, $stat)) {
            throw new \InvalidArgumentException('Invalid option requested');
        }

        return $stat[$option];
    }

    private function getFileName(string $key): string
    {
        return sys_get_temp_dir() . '/' . $this->prefix . '.' . $this->cleanKey($key) . '.cache';
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    private function cleanKey(string $key): string
    {
        return str_replace('.', '', $key);
    }
}
