<?php

namespace App\Cache;

use App\Cache\Exceptions\CacheNotFoundException;
use App\Cache\Exceptions\CacheTimeoutException;
use App\Cache\Exceptions\CacheWriteException;

interface Cache
{
    /**
     * @param string $key
     *
     * @return string
     * @throws CacheTimeoutException
     * @throws CacheNotFoundException
     */
    public function get(string $key): string;

    /**
     * @param string $key
     */
    public function touch(string $key): void;

    /**
     * @param string $key
     * @param string $data
     *
     * @throws CacheWriteException
     */
    public function set(string $key, string $data): void;
}
