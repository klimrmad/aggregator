<?php

/**
 * @see https://www.kraken.com/en-us/help/api
 * TODO: add requests limiting
 */

namespace App;

use App\Cache\Cache;
use App\Cache\Exceptions\CacheNotFoundException;
use App\Cache\Exceptions\CacheTimeoutException;
use App\Cache\Exceptions\CacheWriteException;
use App\HttpClient\Exceptions\HttpRequestFailed;
use App\HttpClient\HttpClient;
use App\HttpClient\HttpCodes;
use InvalidArgumentException;

class Kraken
{

    /**
     * @var string
     */
    private $pair;

    /**
     * @var int
     */
    private $count = 0;

    /**
     * @var HttpClient
     */
    private $client;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * Kraken constructor.
     *
     * @param HttpClient $client
     * @param Cache      $cache
     */
    public function __construct(HttpClient $client, Cache $cache)
    {
        $this->client = $client;
        $this->cache  = $cache;

        $this->init();
    }

    public function run(): void
    {
        $data = $this->getData();

        header('Content-Type: application/json');
        echo $data;
        die;
    }

    private function init(): void
    {
        try {
            $this->initPair();
            $this->initCount();
        } catch (InvalidArgumentException $e) {
            header('HTTP/1.0 ' . HttpCodes::description(HttpCodes::CE__NOT_FOUND));
            die(1);
        }
    }

    private function initPair(): void
    {
        if (!\array_key_exists('pair', $_GET)) {
            throw new InvalidArgumentException('Required parameter not found');
        }

        $this->pair = preg_replace('/[^a-zA-Z0-9]/', '', $_GET['pair']);
        if (!$this->pair) {
            throw new InvalidArgumentException('Required parameter is empty');
        }
    }

    private function initCount(): void
    {
        if (!\array_key_exists('count', $_GET)) {
            return;
        }

        $count       = (int) $_GET['count'];
        $this->count = $count >= 0 ? $count : 0;
    }

    private function getData(): string
    {
        try {
            return $this->cache->get($this->pair);
        } catch (CacheNotFoundException $e) {
        } catch (CacheTimeoutException $e) {
        }

        try {
            $this->cache->touch($this->pair);
            $data = $this->client->get(
                'Depth',
                [
                    'pair'  => $this->pair,
                    'count' => $this->count,
                ]
            );

        } catch (HttpRequestFailed $e) {
            header('HTTP/1.0 ' . HttpCodes::description(HttpCodes::SE__BAD_GATEWAY));
            die(2);
        }

        try {
            $this->cache->set($this->pair, $data);
        } catch (CacheWriteException $e) {
        }

        return $data;
    }

}
