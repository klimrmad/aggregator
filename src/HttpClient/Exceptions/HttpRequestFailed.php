<?php

namespace App\HttpClient\Exceptions;

/**
 * Class HttpRequestFailed
 */
class HttpRequestFailed extends \Exception
{
}
