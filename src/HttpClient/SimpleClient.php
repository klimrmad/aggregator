<?php

namespace App\HttpClient;

use App\HttpClient\Exceptions\HttpRequestFailed;

class SimpleClient implements HttpClient
{

    /**
     * @var string
     */
    private $baseUri;

    /**
     * SimpleClient constructor.
     *
     * @param string $baseUri
     */
    public function __construct(string $baseUri)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * @inheritdoc
     */
    public function get(string $method, array $params = []): string
    {
        $context = stream_context_create(
            [
                'http' => [
                    'method'  => 'GET',
                    'header'  => $this->prepareHeaders(),
                    'timeout' => 60,
                ],
            ]
        );

        try {
            $response = file_get_contents($this->prepareUrl($method, $params), false, $context);
        } catch (\Exception $e) {
            throw new HttpRequestFailed('Request failed');
        }

        if (false === $response) {
            throw new HttpRequestFailed('Request failed');
        }

        return $response;
    }

    /**
     * @return string
     */
    private function prepareHeaders(): string
    {
        $headers = [
            'Accept-Language' => 'en',
            'Accept'          => 'application/json',
        ];

        $res = join(
            "\r\n",
            array_map(
                function ($key, $val) {
                    return "{$key}: {$val}";
                },
                array_keys($headers),
                $headers
            )
        );

        return $res;
    }

    /**
     * @param string $method
     * @param array  $params
     *
     * @return string
     */
    private function prepareUrl(string $method, array $params): string
    {
        $url = $this->baseUri . $method;

        $query = join(
            '&',
            array_map(
                function ($key, $val) {
                    return "{$key}={$val}";
                },
                array_keys($params),
                $params
            )
        );

        $url = join('?', [$url, $query]);

        return $url;
    }

}
