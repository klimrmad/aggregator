<?php

namespace App\HttpClient;

use App\HttpClient\Exceptions\HttpRequestFailed;

interface HttpClient
{
    /**
     * @param string $method
     * @param array  $params
     *
     * @return string
     * @throws HttpRequestFailed
     */
    public function get(string $method, array $params = []): string;
}
