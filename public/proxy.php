<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\Cache\FileCache;
use App\HttpClient\SimpleClient;
use App\Kraken;

$kraken = new Kraken(
    new SimpleClient('https://api.kraken.com/0/public/'),
    new FileCache('kraken', 5)
);
$kraken->run();
