function getURLParam(key, defVal) {
    let values = [];
    let target = location.href;

    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

    let pattern = key + '=([^&#]+)';
    let o_reg   = new RegExp(pattern, 'ig');
    while (true) {
        let matches = o_reg.exec(target);
        if (matches && matches[1]) {
            values.push(matches[1]);
        } else {
            break;
        }
    }

    return values.length ? values : defVal;
}