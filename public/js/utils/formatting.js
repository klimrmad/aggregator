function formatXch(list) {
    if (list.length === 0) {
        return '';
    }
    list.sort();
    return '<xch>' + list.join('</xch><xch>') + '</xch>';
}

function formatNum(number, fracDigits) {
    try {
        if (0 === number) {
            return '';
        }

        let str = number.toLocaleString(
            'arab',
            {
                useGrouping:           false,
                minimumFractionDigits: fracDigits,
                maximumFractionDigits: fracDigits,
            },
        );

        str = '<dec>' + str.replace('.', '</dec><frac>.') + '</frac>';
        if (number < 0) str = '<neg>' + str + '</neg>';
        return str;
    }
    catch (e) {
        return '';
    }
}

function pairFracDigits(pair) {
    switch (pair) {
        case 'btcusd':
            return 5;
        case 'btceur':
            return 5;
        case 'ethbtc':
            return 8;
        case 'ethusd':
            return 4;
    }
    return 6;
}