"use strict";

let collector = {
    config:    {
        maxDepth:  25,
        maxSum:    0,
        volAvg:    0,
        renderMs:  1000,
        graphMode: false,
    },
    providers: {
        F: bitfinex,
        S: bitstamp,
        L: bitlish,
        G: gdax,
        K: kraken,
    },
    pairs:     {},
};

collector.init = function (config, xchList, pairList) {
    function initConfig() {
        for (let id in config) {
            if (!config.hasOwnProperty(id)) {
                continue;
            }
            if (undefined === collector.config[id]) {
                continue;
            }

            collector.config[id] = config[id];
        }
    }

    function initPairs() {
        for (let pId of pairList) {
            collector.pairs[pId] = {
                id:      pId,
                spread:  null,
                avgBuy:  null,
                avgSell: null,
                bid:     {},
                ask:     {},
            };
        }
    }

    function initProviders() {
        for (let xId of xchList) {
            if (undefined === collector.providers[xId]) {
                console.error('Collector. Unknown provider: ', xId);
                continue;
            }

            let provider = collector.providers[xId];
            if (provider.initialized) {
                continue;
            }

            provider.init(pairList);
            provider.onResetData   = collector.resetData;
            provider.onRemovePrice = collector.removePrice;
            provider.onUpdatePrice = collector.updatePrice;
        }
    }

    initConfig();
    initPairs();
    initProviders();
};

collector.start = function () {
    for (let xId in collector.providers) {
        if (!collector.providers.hasOwnProperty(xId)) {
            continue;
        }

        let provider = collector.providers[xId];
        if (!provider.initialized) {
            continue;
        }

        setTimeout(provider.start, 1);
    }
};

/**
 * @param xId  string      Exchange Id
 * @param pId  string      Pair Id
 * @param side null|string Side
 *
 * @return void
 */
collector.resetData = function (xId, pId, side = null) {
    function resetSide(side) {
        if (undefined === collector.pairs[pId][side]) {
            return;
        }
        for (let priceId in collector.pairs[pId][side]) {
            if (!collector.pairs[pId][side].hasOwnProperty(priceId)) {
                continue;
            }
            if (undefined === collector.pairs[pId][side][priceId].xch) {
                continue;
            }
            if (undefined === collector.pairs[pId][side][priceId].xch[xId]) {
                continue;
            }

            delete collector.pairs[pId][side][priceId].xch[xId];
            if (0 === Object.keys(collector.pairs[pId][side][priceId].xch).length) {
                delete collector.pairs[pId][side][priceId];
            }
        }
    }

    if (undefined === collector.pairs[pId]) {
        return;
    }

    if (null != side) {
        resetSide(side);
        return;
    }

    resetSide('bid');
    resetSide('ask');
};

/**
 * @param xId   string Exchange Id
 * @param pId   string Pair Id
 * @param side  string
 * @param price float
 *
 * @return void
 */
collector.removePrice = function (xId, pId, side, price) {
    if (undefined === collector.pairs[pId][side]) {
        return;
    }
    if (undefined === collector.pairs[pId][side][price]) {
        return
    }

    delete collector.pairs[pId][side][price].xch[xId];
    if (0 === Object.keys(collector.pairs[pId][side][price].xch).length) {
        delete collector.pairs[pId][side][price];
    }
};

/**
 * @param xId       string
 * @param pId       string
 * @param side      string
 * @param price     float
 * @param volume    float
 * @param timestamp null|integer
 *
 * @return void
 */
collector.updatePrice = function (xId, pId, side, price, volume, timestamp = null) {
    if (undefined === collector.pairs[pId][side]) {
        collector.pairs[pId][side] = {};
    }

    if (undefined === collector.pairs[pId][side][price]) {
        collector.pairs[pId][side][price] = {
            id:    price,
            price: parseFloat(price),
            xch:   {},
            ts:    0,
        };
    }

    collector.pairs[pId][side][price].xch[xId] = volume;

    collector.pairs[pId][side][price].ts =
        null != timestamp
            ? timestamp
            : +new Date();
};

collector.getPairData = function (pairId) {
    function getShortList(source, sortFunc) {

        let keys = Object.keys(source);
        if (0 === keys.length) {
            return [];
        }
        keys.sort(sortFunc);

        let res = [];
        let sum = 0;
        let cnt = 0;
        let agg = 0;
        for (let id of keys) {
            let vol = 0;
            let xch = [];

            for (let xchId in source[id].xch) {
                if (source[id].xch.hasOwnProperty(xchId)) {
                    xch.push(xchId);
                    vol += source[id].xch[xchId];
                    sum += source[id].xch[xchId];
                }
            }
            agg += vol * source[id].price;

            res[id] = {
                id:    source[id].id,
                price: source[id].price,
                ts:    source[id].ts,
                vol:   vol,
                sum:   sum,
                avg:   agg / sum,
                xch:   xch,
            };

            if (market.config.maxDepth && ++cnt >= market.config.maxDepth) {
                return res;
            }
            if (market.config.maxSum && sum > market.config.maxSum) {
                return res;
            }
        }
        return res;
    }

    function getFirstElement(data, sortFunc) {
        let keys = Object.keys(data);
        keys.sort(sortFunc);
        return (keys.length > 0)
            ? data[keys[0]]
            : null;
    }

    function getAveragePrice(data, sortFunc) {
        if (!market.config.volAvg) {
            return 0;
        }
        let keys = Object.keys(data);
        keys.sort(sortFunc);
        for (let id of keys) {
            if (data[id].sum >= market.config.volAvg) {
                return data[id].avg;
            }
        }
        return 0;
    }

    function getMerged() {
        let merged = {};
        let keys;

        function getOne(data) {
            let priceId = data.id;
            if (undefined === merged[priceId]) {
                merged[priceId] = {
                    id:     data.id,
                    price:  data.price,
                    ts:     data.ts,
                    bidVol: 0,
                    bidSum: 0,
                    bidXch: [],
                    askVol: 0,
                    askSum: 0,
                    askXch: [],
                };
            }
            return merged[priceId];
        }

        keys = Object.keys(bid);
        for (let id of keys) {
            let one    = getOne(bid[id]);
            one.bidVol = bid[id].vol;
            one.bidSum = bid[id].sum;
            one.bidXch = bid[id].xch;
        }

        keys = Object.keys(ask);
        for (let id of keys) {
            let oneMerged    = getOne(ask[id]);
            oneMerged.askVol = ask[id].vol;
            oneMerged.askSum = ask[id].sum;
            oneMerged.askXch = ask[id].xch;
        }

        return merged;
    }

    function getSpread() {
        let maxBid = getFirstElement(bid, descByFloat);
        let minAsk = getFirstElement(ask, ascByFloat);
        try {
            return minAsk.price - maxBid.price;
        } catch (e) {
            return 0;
        }
    }

    if (undefined === collector.pairs[pairId]) {
        return null;
    }

    let pair = collector.pairs[pairId];
    let bid  = getShortList(pair.bid, descByFloat);
    let ask  = getShortList(pair.ask, ascByFloat);

    return {
        merged:  getMerged(),
        spread:  getSpread(),
        avgBuy:  getAveragePrice(bid, descByFloat),
        avgSell: getAveragePrice(ask, ascByFloat),
    };
};

