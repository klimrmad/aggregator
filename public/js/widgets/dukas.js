let dukas = {
    id: 'dukas',
    config: {
        type: "marketwatch",
        params: {
            showHeader: false,
            live: true,
            columns: 1,
            pips: 0,
            availableInstruments: "l:",
            selectorInstrumentsList: "l:EUR/USD,USD/JPY,GBP/USD,USD/RUB,EUR/RUB",
            mode: 0,
            background: "#ffffff",
            cellTopBorderColor: "#7D9EC7",
            instrumentSelectorBackground: "#454545",
            instrumentFontColor: "#FFFFFF",
            width: "425",
            height: "900",
            adv: "popup",
            lang: "ru",
        },
    }
};

dukas.render = function () {
    let $content = document.createElement('div');

    let $widget = dukasCore(dukas.config);
    $content.appendChild($widget);

    return $content;
};
