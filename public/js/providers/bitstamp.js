"use strict";

/**
 * @see https://www.bitstamp.net/websocket
 */
let bitstamp = {
    id:            'S',
    initialized:   false,
    //
    key:           'de504dc5763aeef9ff52',
    pusher:        null,
    //
    pairs:         {},
    //
    onResetData:   null,
    onUpdatePrice: null,
    onRemovePrice: null,
};

bitstamp.init = function (pairList) {
    if (bitstamp.initialized) {
        return;
    }

    for (let pairId of pairList) {
        bitstamp.pairs[pairId.toLowerCase()] = pairId;
    }

    bitstamp.initialized = true;
};

bitstamp.start = function () {
    if (!bitstamp.initialized) {
        return;
    }

    bitstamp.pusher = new Pusher(bitstamp.key);

    for (let localId in bitstamp.pairs) {
        if (!bitstamp.pairs.hasOwnProperty(localId)) {
            continue;
        }

        bitstamp.subscribe('order_book', localId, true);
        bitstamp.subscribe('diff_order_book', localId);
    }
};

bitstamp.subscribe = function (channel, localId, runOnce = false) {
    function processOne(one, side) {
        let price  = parseFloat(one[0]);
        let amount = parseFloat(one[1]);

        let callback =
                (0 === amount)
                    ? 'onRemovePrice'
                    : 'onUpdatePrice';

        bitstamp[callback](bitstamp.id, bitstamp.pairs[localId], side, price, amount);
    }

    function processList(list, side) {
        for (let one of list) {
            processOne(one, side);
        }
    }

    function onData(data) {
        if (undefined !== data['bids']) {
            processList(data['bids'], 'bid');
        }

        if (undefined !== data['asks']) {
            processList(data['asks'], 'ask');
        }

        if (runOnce) {
            subscription.unbind('data', onData);
            subscription.unsubscribe();
        }
    }

    let subscription = bitstamp.pusher.subscribe(channel + (localId === 'btcusd' ? '' : '_' + localId));
    subscription.bind('data', onData);
};
