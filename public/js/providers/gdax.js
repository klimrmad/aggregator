"use strict";

/**
 * @see https://www.gdax.net/websocket
 */
let gdax = {
    id:            'G',
    initialized:   false,
    //
    baseUri:       'wss://ws-feed.gdax.com',
    ws:            null,
    //
    pairs:         {},
    //
    onResetData:   null,
    onUpdatePrice: null,
    onRemovePrice: null,
};

gdax.init = function (pairList) {
    if (gdax.initialized) {
        return;
    }

    for (let pairId of pairList) {
        let localId         = (pairId.substr(0, 3) + '-' + pairId.substr(3)).toUpperCase();
        gdax.pairs[localId] = pairId;
    }

    gdax.initialized = true;
};

gdax.start = function () {
    if (gdax.initialized) {
        gdax.connect();
    }
};

gdax.connect = function () {
    gdax.ws           = new WebSocket(gdax.baseUri);
    gdax.ws.onmessage = gdax.onMessage;
    gdax.ws.onopen    = function () {
        function subscribe(localId) {
            let json = {
                type:        "subscribe",
                product_ids: [localId],
                channels:    ["level2"],
            };
            gdax.ws.send(JSON.stringify(json));
        }

        for (let localId in gdax.pairs) {
            if (gdax.pairs.hasOwnProperty(localId)) {
                subscribe(localId);
            }
        }
    }
};

gdax.onMessage = function (evt) {
    function snapshot(data) {
        function processOne(one, side) {
            let price  = parseFloat(one[0]);
            let amount = parseFloat(one[1]);

            let callback =
                    (0 === amount)
                        ? 'onRemovePrice'
                        : 'onUpdatePrice';

            gdax[callback](gdax.id, pairId, side, price, amount);
        }

        function processList(list, side) {
            for (let one of list) {
                processOne(one, side);
            }
        }

        let localId = data['product_id'];
        if (undefined === gdax.pairs[localId]) {
            console.warn('Bitfinex. Unknown pair: ', localId, data);
            return;
        }

        let pairId = gdax.pairs[localId];

        if (undefined !== data['bids']) {
            processList(data['bids'], 'bid');
        }

        if (undefined !== data['asks']) {
            processList(data['asks'], 'ask');
        }
    }

    function update(data) {
        function processOne(one) {

            let side;
            switch (one[0]) {
                case 'buy':
                    side = 'bid';
                    break;
                case 'sell':
                    side = 'ask';
                    break;
                default:
                    console.warn('Bitfinex. Unknown side ', one[0], one);
                    return;
            }

            let price  = parseFloat(one[1]);
            let amount = parseFloat(one[2]);

            let callback =
                    (0 === amount)
                        ? 'onRemovePrice'
                        : 'onUpdatePrice';

            gdax[callback](gdax.id, pairId, side, price, amount);
        }

        let localId = data['product_id'];
        if (undefined === gdax.pairs[localId]) {
            console.warn('Bitfinex. Unknown pair: ', localId, data);
            return;
        }

        let pairId = gdax.pairs[localId];

        if (undefined !== data['changes']) {
            for (let one of data['changes']) {
                processOne(one)
            }
        }
    }

    if (undefined === evt.data) {
        console.debug('Gdax. Unknown event: ', evt);
        return;
    }

    let eventData = JSON.parse(evt.data);

    if (undefined !== eventData.type) {
        switch (eventData.type) {
            case 'snapshot' :
                snapshot(eventData);
                return;
            case 'l2update' :
                update(eventData);
                return;
            case 'subscriptions':
                return;
            default:
                console.debug('Gdax. Unknown event: ', eventData);
                return;
        }
    }

    console.debug('Gdax. Unknown event: ', evt);
};
