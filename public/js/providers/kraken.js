"use strict";

/**
 * @see https://www.kraken.com/en-us/help/api
 */
let kraken = {
    id:             'K',
    initialized:    false,
    //
    baseUri:        '/proxy.php',
    requestDelayMs: 750 + 500 * Math.random(),
    //
    pairs:          {},
    //
    onResetData:    null,
    onUpdatePrice:  null,
    onRemovePrice:  null,
    //
    currencyMap:    {
        btc: 'x' + 'xbt',
        eth: 'x' + 'eth',
        ltc: 'x' + 'ltc',
        usd: 'z' + 'usd',
        eur: 'z' + 'eur',
        cad: 'z' + 'cad',
        gbp: 'z' + 'gbp',
        jpy: 'z' + 'jpy',
    },
};

kraken.init = function (pairList) {
    if (kraken.initialized) {
        return;
    }

    function initPairs() {
        function getCurrencyId(commonId) {
            commonId = commonId.toLowerCase();
            return (
                undefined !== kraken.currencyMap[commonId]
                    ? kraken.currencyMap[commonId]
                    : commonId
            ).toUpperCase();
        }

        function getLocalPairId(pairId) {
            return getCurrencyId(pairId.substr(0, 3)) + getCurrencyId(pairId.substr(3, 3));
        }

        for (let pairId of pairList) {
            kraken.pairs[getLocalPairId(pairId)] = pairId;
        }
    }

    initPairs();

    kraken.initialized = true;
};

kraken.start = function () {
    if (kraken.initialized) {
        kraken.receive();
    }
};

kraken.receive = function () {
    function receiveOne(localId, localList) {
        function requestNext() {
            if (0 === localList.length) {
                localList = Object.keys(kraken.pairs);
            }

            localId = localList.shift();

            setTimeout(
                function () {
                    receiveOne(localId, localList);
                },
                kraken.requestDelayMs,
            );
        }

        let xmlHttp = new XMLHttpRequest();

        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState !== 4) {
                return;
            }

            console.debug('Kraken. Response');

            if (xmlHttp.status !== 200) {
                console.error('Kraken. Invalid response: ', xmlHttp.response);

                kraken.onResetData(kraken.id, kraken.pairs[localId]);

                requestNext();
                return;
            }

            let responseData = JSON.parse(xmlHttp.responseText);
            if (responseData.error && responseData.error.length) {
                console.error('Kraken. Error response: ', responseData.error);

                kraken.onResetData(kraken.id, kraken.pairs[localId]);

                requestNext();
                return;
            }

            if (responseData.result) {
                kraken.onSnapshot(responseData.result);
            }

            requestNext();
        };

        let url = kraken.baseUri + '?pair=' + localId + '&count=100';
        console.debug('Kraken. Request: ', url);
        xmlHttp.open('GET', url, true);
        xmlHttp.send(null);
    }

    let localList = Object.keys(kraken.pairs);
    let localId   = localList.shift();

    receiveOne(localId, localList);
};

kraken.onSnapshot = function (data) {
    function pushData(pairId, data, side) {
        if (!Array.isArray(data)) {
            return;
        }

        kraken.onResetData(kraken.id, pairId, side);

        for (let one of data) {
            let price  = parseFloat(one[0]);
            let volume = parseFloat(one[1]);
            let ts     = 0; // could be one[2]*1000 but there is dates in the future and no description for this field in manual
            kraken.onUpdatePrice(kraken.id, pairId, side, price, volume, ts);
        }
    }

    for (let localId in data) {
        if (!data.hasOwnProperty(localId)) {
            continue;
        }
        if (undefined === kraken.pairs[localId]) {
            continue;
        }

        let pairId = kraken.pairs[localId];

        pushData(pairId, data[localId].asks, 'ask');
        pushData(pairId, data[localId].bids, 'bid');
    }
};
