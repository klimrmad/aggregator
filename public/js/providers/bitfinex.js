"use strict";

/**
 * @see https://docs.bitfinex.com/docs/ws-general
 */
let bitfinex = {
    id:            'F',
    initialized:   false,
    //
    baseUri:       "wss://api.bitfinex.com/ws/",
    ws:            null,
    //
    pairs:         {},
    subscriptions: {},
    //
    onResetData:   null,
    onUpdatePrice: null,
    onRemovePrice: null,
};

bitfinex.init = function (pairList) {
    if (bitfinex.initialized) {
        return;
    }

    for (let pairId of pairList) {
        bitfinex.pairs[pairId.toUpperCase()] = pairId;
    }

    bitfinex.initialized = true;
};

bitfinex.start = function () {
    if (bitfinex.initialized) {
        bitfinex.connect();
    }
};

bitfinex.connect = function () {
    bitfinex.ws = new WebSocket(bitfinex.baseUri);

    bitfinex.ws.onmessage = bitfinex.onMessage;
    bitfinex.ws.onopen    = function () {
        function subscribe(pairId) {
            let json = {
                event:   'subscribe',
                channel: 'book',
                pair:    pairId,
                prec:    "P0",
                len:     100,
            };
            bitfinex.ws.send(JSON.stringify(json));
        }

        for (let pairId in bitfinex.pairs) {
            if (bitfinex.pairs.hasOwnProperty(pairId)) {
                subscribe(pairId);
            }
        }
    }
};

bitfinex.onMessage = function (evt) {
    function subscription(data) {
        console.debug('Bitfinex. Subscribed: ', data);

        let pairId = data.pair;
        if (undefined === bitfinex.pairs[pairId]) {
            console.warn('Bitfinex. Unexpected channel: ', data);
            return;
        }

        let chanId = data.chanId;

        bitfinex.subscriptions[chanId] = bitfinex.pairs[pairId];
    }

    function data(chanId, data) {
        function processOne(one) {
            let price  = one[0];
            let count  = one[1];
            let amount = Math.abs(one[2]);
            let side   = one[2] > 0 ? 'bid' : 'ask';

            let callback =
                    (0 === count)
                        ? 'onRemovePrice'
                        : 'onUpdatePrice';

            bitfinex[callback](bitfinex.id, pairId, side, price, amount);
        }

        if (undefined === bitfinex.subscriptions[chanId]) {
            console.warn('Bitfinex. Unsubscribed channel: ', chanId, data);
            return;
        }
        let pairId = bitfinex.subscriptions[chanId];

        if (Array.isArray(data[1])) {
            for (let one of data[1]) {
                processOne(one);
            }
            return;
        }

        data.shift();
        processOne(data);
    }

    if (undefined === evt.data) {
        console.debug('Bitfinex. Unknown event: ', evt);
        return;
    }

    let eventData = JSON.parse(evt.data);

    if (undefined !== eventData[0]) {
        if ('hb' !== eventData[1]) {
            data(eventData[0], eventData);
        }
        return;
    }

    if (undefined !== eventData.event) {
        switch (eventData.event) {
            case 'info' :
                console.info('Bitfinex. Info: ', eventData);
                return;
            case 'subscribed' :
                subscription(eventData);
                return;
            default:
                console.debug('Bitfinex. Unknown event: ', eventData);
                return;
        }
    }

    console.debug('Bitfinex. Unknown event: ', evt);
};

