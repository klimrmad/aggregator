"use strict";

/**
 * @see https://bitlish.com/api/ru#orderbook
 */
let bitlish = {
    id:            'L',
    initialized:   false,
    //
    baseUri:       "wss://bitlish.com/ws",
    ws:            null,
    //
    pairs:         {},
    //
    onResetData:   null,
    onUpdatePrice: null,
    onRemovePrice: null,
};

bitlish.init = function (pairList) {
    if (bitlish.initialized) {
        return;
    }

    for (let pairId of pairList) {
        bitlish.pairs[pairId.toLowerCase()] = pairId;
    }

    bitlish.initialized = true;
};

bitlish.start = function () {
    if (bitlish.initialized) {
        bitlish.connect();
    }
};

bitlish.connect = function () {
    bitlish.ws           = new WebSocket(bitlish.baseUri);
    bitlish.ws.onmessage = bitlish.onMessage;
    bitlish.ws.onopen    = function () {
        function subscribe(pairId) {
            let json = {
                type:    'request',
                token:   null,
                mark:    null,
                data:    null,
                call:    'trades_depth',
                pair_id: pairId,
            };
            bitlish.ws.send(JSON.stringify(json));
        }

        for (let pairId in bitlish.pairs) {
            if (bitlish.pairs.hasOwnProperty(pairId)) {
                subscribe(pairId);
            }
        }
    }
};

bitlish.onMessage = function (evt) {
    function onTradesDepth(data) {
        function processOne(pairId, one, side) {
            let price  = parseFloat(one['price']);
            let volume = parseFloat(one['volume']);

            bitlish.onUpdatePrice(bitlish.id, pairId, side, price, volume);
        }

        function processList(pairId, data, side) {
            if (undefined === data[side]) {
                return;
            }

            for (let one of data[side]) {
                processOne(pairId, one, side);
            }
        }

        for (let localId in bitlish.pairs) {
            if (!bitlish.pairs.hasOwnProperty(localId)) {
                continue;
            }
            let pairId = bitlish.pairs[localId];

            if (undefined === data[localId]) {
                continue;
            }

            processList(pairId, data[localId], 'bid');
            processList(pairId, data[localId], 'ask');
        }
    }

    function onOrderInfo(data) {
        let localId = data.order['pair_id'];
        if (undefined === bitlish.pairs[localId]) {
            return;
        }
        let pairId = bitlish.pairs[localId];

        let price  = parseFloat(data.order['price']);
        let volume = parseFloat(data.order['amount_free']);
        let side   = data.order['dir'];

        let callback;
        switch (data.order['state']) {
            case 'new':
            case 'part':
                callback = 'onUpdatePrice';
                break;
            case 'done':
            case 'cancel':
                callback = 'onRemovePrice';
                break;
            default:
                console.error('Bitlish. Unknown order state: ', data.order['state'], data);
                return;
        }

        bitlish[callback](bitlish.id, pairId, side, price, volume);
    }

    if (undefined === evt.data) {
        console.debug('Bitlish. Unknown event: ', evt);
        return;
    }

    let eventData = JSON.parse(evt.data);

    if (undefined !== eventData.call) {
        switch (eventData.call) {
            case 'public_trade_order_create' :
            case 'public_trade_order_match' :
            case 'public_trade_order_cancel_expire' :
            case 'public_trade_order_cancel' :
                onOrderInfo(eventData['data']);
                return;
            case 'trades_depth' :
                onTradesDepth(eventData['data']);
                return;
            case 'tickers' :
            case 'ohlcv' :
                return;
            default:
                console.debug('Bitlish. Unknown event: ', eventData);
                return;
        }
    }

    console.debug('Bitlish. Unknown event: ', evt);
};

