"use strict";

let market = {
    root:   null,
    //
    pairs:    {},
    widgets:  {},
    //
    config: {
        maxDepth:  25,
        maxSum:    0,
        volAvg:    0,
        renderMs:  1000,
        graphMode: false,
    },
};

market.init = function (config, pairList, widgetList) {
    function initConfig() {
        for (let id in config) {
            if (!config.hasOwnProperty(id)) {
                continue;
            }
            if (undefined === market.config[id]) {
                continue;
            }

            market.config[id] = config[id];
        }
    }

    function initRoot() {
        let $root = document.createElement('div');

        $root.id        = 'market';
        $root.className =
            market.config.graphMode
                ? 'graph'
                : '';

        document.body.appendChild($root);

        market.root = $root;
    }

    function initPairs() {
        function initElements(pairId) {
            function createHeader() {
                function createCell(className, value) {
                    let $cell = document.createElement('th');

                    $cell.className = className;
                    $cell.innerText = value;

                    return $cell;
                }

                let $row = document.createElement('tr');
                $row.appendChild(createCell('buy xch', 'xch'));
                $row.appendChild(createCell('buy sum', 'sum'));
                $row.appendChild(createCell('buy volume', 'vol'));
                $row.appendChild(createCell('price', 'price'));
                $row.appendChild(createCell('sell volume', 'vol'));
                $row.appendChild(createCell('sell sum', 'sum'));
                $row.appendChild(createCell('sell xch', 'xch'));
                return $row;
            }

            let $title = document.createElement('h1');
            $title.addEventListener('click', function () {
                market.config.graphMode = !market.config.graphMode;
                market.root.className   = market.config.graphMode ? 'graph' : '';
            });
            $title.innerText =
                pairId.length === 6
                    ? pairId.substr(0, 3) + '/' + pairId.substr(3)
                    : pairId;

            let $spread       = document.createElement('h2');
            $spread.title     = 'Spread';
            $spread.className = 'spread num blue';

            let $avgBuy       = document.createElement('h2');
            $avgBuy.title     = 'Average buy price for ' + market.config.volAvg;
            $avgBuy.className = 'avgBuy num green';

            let $avgSell       = document.createElement('h2');
            $avgSell.title     = 'Average sell price for ' + market.config.volAvg;
            $avgSell.className = 'avgSell num red';

            let $header       = document.createElement('div');
            $header.className = 'header';
            $header.appendChild($title);
            $header.appendChild($avgBuy);
            $header.appendChild($spread);
            $header.appendChild($avgSell);

            let $body       = document.createElement('tbody');
            $body.innerHTML = '<tr><td colspan="7">Loading...</td></tr>';

            let $table = document.createElement('table');
            $table.appendChild(createHeader());
            $table.appendChild($body);

            let $scroll       = document.createElement('div');
            $scroll.className = 'scroll';
            $scroll.appendChild($table);

            let $container = document.createElement('div');
            $container.id  = pairId;
            $container.appendChild($header);
            $container.appendChild($scroll);

            market.root.appendChild($container);
            market.pairs[pairId].body    = $body;
            market.pairs[pairId].spread  = $spread;
            market.pairs[pairId].avgBuy  = $avgBuy;
            market.pairs[pairId].avgSell = $avgSell;
        }

        for (let pairId of pairList) {

            market.pairs[pairId] = {
                id:        pairId,
                rendering: false,
                //
                body:      null,
                spread:    null,
                avgBuy:    null,
                avgSell:   null,
            };

            initElements(pairId);
        }
    }

    function initWidgets() {
        function initWidgetRoot(id) {
            let $root = document.createElement('div');
            $root.id = widgetList[id].widget.id;

            let $header = document.createElement('div');
            $header.className = 'header';
            $header.innerHTML = '<h1>' + widgetList[id].widget.id + '</h1>';
            $root.appendChild($header);

            market.widgets[id].root = $root;
            market.root.appendChild($root);
        }

        function removeWidget(id) {
            if (market.widgets[id].body) {
                market.widgets[id].body.remove();
            }
        }

        function loadWidget(id) {
            market.widgets[id].body = market.widgets[id].widget.render();
            market.widgets[id].root.appendChild(market.widgets[id].body);
        }

        for (let id in widgetList) {
            if (!widgetList.hasOwnProperty(id)) {
                continue;
            }

            market.widgets[id] = {
                id:       id,
                //
                widget: widgetList[id].widget,
                reload: widgetList[id].reload * 1000,
                //
                root:   null,
                body:   null,
            };

            initWidgetRoot(id);
            loadWidget(id);

            if (market.widgets[id].reload > 0) {
                setInterval(
                    function () {
                        removeWidget(id);
                        loadWidget(id);
                    },
                    market.widgets[id].reload
                );
            }
        }
    }

    initConfig();
    initRoot();
    initPairs();
    initWidgets();
};

market.start = function () {
    setInterval(market.render, market.config.renderMs);
};

market.render = function () {
    function renderPair(pair) {
        function renderBody(merged) {
            let ts = +new Date();

            function renderRow(data) {
                function createCell(className, innerHTML) {
                    let cell       = document.createElement('td');
                    cell.className = className;
                    cell.innerHTML = innerHTML;
                    row.appendChild(cell);
                }

                let row = document.createElement('tr');

                row.dataset.age = (Math.floor((ts - data.ts) / 1000)).toString();

                createCell('green buy xch', formatXch(data.bidXch));
                createCell('green buy sum num', formatNum(data.bidSum, 4));
                createCell('green buy volume num', formatNum(data.bidVol, 4));
                createCell('blue price num', formatNum(data.price, pairFracDigits(pair.id)));
                createCell('red sell volume num', formatNum(data.askVol, 4));
                createCell('red sell sum num', formatNum(data.askSum, 4));
                createCell('red sell xch', formatXch(data.askXch));

                return row;
            }

            pair.body.innerHTML = '';

            let keys = Object.keys(merged);
            keys.sort(descByFloat);
            for (let id of keys) {
                pair.body.appendChild(renderRow(merged[id]));
            }
        }

        function renderHeader(spread, avgBuy, avgSell) {
            let frac               = pairFracDigits(pair.id);
            pair.spread.innerHTML  = formatNum(spread, frac);
            pair.avgBuy.innerHTML  = formatNum(avgBuy, frac);
            pair.avgSell.innerHTML = formatNum(avgSell, frac);
        }

        let data = collector.getPairData(pair.id);

        renderBody(data.merged);
        renderHeader(data.spread, data.avgBuy, data.avgSell);
    }

    for (let pairId in market.pairs) {
        if (!market.pairs.hasOwnProperty(pairId)) {
            continue;
        }

        if (market.pairs[pairId].rendering) {
            continue;
        }

        market.pairs[pairId].rendering = true;
        renderPair(market.pairs[pairId]);
        market.pairs[pairId].rendering = false;
    }
};
